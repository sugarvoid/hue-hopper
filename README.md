# Hue Hopper
#### Version 0.4.0

![](https://gitlab.com/sugarvoid/hue-hopper/-/blob/Current/docs/hue.gif)
(Gif is from version 0.3.0)

## Description
A game about rotating and bouncing. This is the first "game" I've made so the code isn't as clean/structured as I'd like. Still slowly working on refactoring as I learn better practices.

## Controls 
- **Move** [A] and [D]
- **Rotate** Arrown Keys
- **Slam** Space
- **Pause** Esc

## Credits

### Art

- Input Prompts Pixel 16× (1.0) - Created/distributed by Kenney (www.kenney.nl)
- Pixel Platformer by [kenny](https://kenney.itch.io/)

### Fonts

- Petita - Manfred Klein 
- Kronika - Apostrophic Labs
- Kultro - Muchammad Wahono Sapto Adi Saputro
- Petita - Manfred Klein
- Wash Your Hand - Syaf Rizal (Khurasan)
- Kronika - Apostrophic Labs
- Want Coffee - Syaf Rizal (Khurasan)

### Sounds

- Coin Pick up - Retro_Coin_04 by Mattias "MATRIXXX" Lahoud [license](https://creativecommons.org/licenses/by/3.0/)
- Correct Sound - COrrect Choice by Unadamlar [license](https://creativecommons.org/publicdomain/zero/1.0/)
- Wrong Sound - old reactor by Leszek_Szary [license](https://creativecommons.org/publicdomain/zero/1.0/)
- Keep Your Cream Alive by [Congusbongus](https://soundcloud.com/congus-bongus) [license](https://creativecommons.org/publicdomain/zero/1.0/)
- Puzzle Pieces - [Ben Burnes](https://www.abstractionmusic.com/tallbeard.htm)
