# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.4.0] - 2022-04-16
### Added
- Spike enemies and flying enemies
- Background color changes with game difficulty
### Changed
- Rounded Hue Ball image
### Fixed
- Bug preventing pause screen being in front of enemy sprite
### Removed
- Button that spawned spikes
- Spikes that fall when player lands on wrong color


## [0.3.1] - 2022-01-11
### Added
- Button that spawns spikes
### Changed
- Spikes spawn when red button is pressed
### Removed
- Player taking damage from enemies

## [0.3.0] - 2021-11-24
### Added
- Kitargo splash screen
- Enemies now drop through ground when killed instead of just vanishing
- Orbs are now the debuff items
- The color of the command label now changes colors
### Changed
- Tree image is now a white panel
- Moved game from 1.3.1 to 0.3.0 since it is still not a full game. Having a full version seemed wrong and could be confusing. 
### Fixed
- Flash effect on the player plays when taking a hit or spike and enemy
### Removed
- Gems are no longer the debuff items

## [0.2.2] - 2021-11-17
### Changed
- Replaced fonts on start menu and tree commands

## [0.2.0] - 2021-11-02
### Added
- Spikes that hurt on contact now fall mores time goes on
- Added difficulty 
- Added debuff gems
- Background color changes with difficulty 
- Added debug
- Added pause screen
- Added point multiplier. Activated after killing enemy
- Added music and sound effects
- Added settings page for turning off sounds and rumble
- Added xbox controller support
- Added controller rumble
- Added local leader-board
- Flash effect when player is hit
- Added screen shake when landing 
### Changed
- Bounce height after landing on enemy is lower
### Removed
- Coins

## [0.2.1] - 2021-08-25
### Added
- Added a slightly better start screen
- Player can now kill enemies by matching the color
- Added sound to game over screen
### Changed
- Changed the background color
- Replaced the blue part of the player to purple
- Changed player sprite. (Gray guy is now in his helmet)
### Removed
- Coin multiplier 

## [0.1.1] - 2021-08-23
### Added
- Background music

## [0.1.0] - 2021-08-21
- Game jam version 