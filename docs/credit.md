# Credits

## Art

Input Prompts Pixel 16× (1.0) - Created/distributed by Kenney (www.kenney.nl)

## Fonts

Kultro - Muchammad Wahono Sapto Adi Saputro
Petita - Manfred Klein
Wash Your Hand - Syaf Rizal (Khurasan)
Kronika - Apostrophic Labs
Want Coffee - Syaf Rizal (Khurasan)

## Sounds

- Puzzle Pieces - [Ben Burnes](https://www.abstractionmusic.com/tallbeard.htm)
